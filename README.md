# WebSpoon Docker Image
###### Based on work done by Hiromu Hota (https://github.com/HiromuHota/webspoon-docker)

## Clone Repository and Build Image

Clone git repository https://gitlab.com/docker.public/webspoon.git

```
git clone https://gitlab.com/docker.public/webspoon.git
```

```
cd webspoon/
docker build -t americatelperu/webspoon:latest .
```

## Run Pentaho Server
```
docker-compose up -d
```