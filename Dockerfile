FROM hiromuhota/webspoon:latest
MAINTAINER Miguel Neyra (mneyra@americatel.com.pe)
# Based on work done by Hiromu Hota (https://github.com/HiromuHota/webspoon-docker)

WORKDIR /tmp

RUN curl -L -o mysql-connector-java-5.1.46.jar http://central.maven.org/maven2/mysql/mysql-connector-java/5.1.46/mysql-connector-java-5.1.46.jar
RUN curl -L -o jtds-1.3.1.jar http://central.maven.org/maven2/net/sourceforge/jtds/jtds/1.3.1/jtds-1.3.1.jar
RUN curl -L -o mssql-jdbc-6.4.0.jre8.jar http://central.maven.org/maven2/com/microsoft/sqlserver/mssql-jdbc/6.4.0.jre8/mssql-jdbc-6.4.0.jre8.jar

RUN cp /tmp/mysql-connector-java-5.1.46.jar /usr/local/tomcat/lib/mysql-connector-java-5.1.46.jar
RUN cp /tmp/jtds-1.3.1.jar /usr/local/tomcat/lib/jtds-1.3.1.jar
RUN cp /tmp/mssql-jdbc-6.4.0.jre8.jar /usr/local/tomcat/lib/mssql-jdbc-6.4.0.jre8.jar